#! /bin/bash


# Bash function to compute a decimal version number.
# If this would be the third git commit, the version
# number will be 0.3
# If this would be the seventeenth git commit, the version
# number will be 1.7
#
# (Copy this function to your ~/.bash_profile if you like)
version() {
    # Check if a filename is provided as an argument
    if [ -z "$1" ]; then
        echo "Usage: version <filename>"
        return 1
    fi

    # Verify file exists...otherwise it would always return 0.1
    if [ ! -e "$1" ]; then
      echo "File does not exist: $1"
      return 1
    fi

    local FILENAME="$1"

    # Get the number of Git commits for the specified filename
    # Note --follow means also follow it through renames
    local NUM_COMMITS=$(git log --follow --oneline -- "$FILENAME" | wc -l)

    # Increment the number of commits by 1 to start from version 1
    ((NUM_COMMITS++))

    # Calculate the tens and ones place
    local TENS_PLACE=$((NUM_COMMITS / 10))
    local ONES_PLACE=$((NUM_COMMITS % 10))

    # Print the computed version number in the format 'a.b'
    echo "Next version: $TENS_PLACE.$ONES_PLACE"
}




# Now that the version function is defined, use it to compute the next version
version blush.f3d
