
Andy v1 (Predecessor to Blush)
-------

First one that plays well! (A happy accident I call it)
It required a shim atop the reed for the ligature to fit.
The tooth rest was high, so to get the best sound it FEELS like you're really
putting a lot of mouthpiece in your mouth
It was hard to seal my lips around the sides of it, so playing was a slobbery
mess.

Andy v2 (sent to PJ)
--------------------

Raised the table so the ligature will fit.
Expanded the bore for an easier fit on the neck cork.

Blush v0.1
----------

Copied "Andy v2" and renamed to "Blush"

Added the name Blush and the version number on the side of the barrel

Reduced the chamber diameter.
	Previously:
		Bore: 17mm
		Right before window: 17mm
		Window Full width: 17mm
	Now:
		Bore: 17mm
		Right before window: 16.2mm wide, 15.2mm high ellipse
		Window Full width: 16mm wide, 15mm high ellipse

Raised the rollover baffle. (Unfortunately the baffle is no longer curved
about the Z axis, so it does not nest in tidily at the tip rail

This version makes big, solid (especially down low!) but is highly resistant.
It also requires you to line your reed up carefully between the rails.
The top end does play, but does not sing like I want it to.
And I would like just a little sizzle instead of only dark.

Blush v0.3
----------

Thinner tip rail, by way of changing the extrusion cut angle to be
perpendicular to the facing curve at the tip rail.

Thinner end wall, which may break off if you push on it too hard.

This version has only moderate resistance (hooray!)
The sound is also a touch brighter than blush v0.1 (hooray!)

Looking forward to sending this one to friends.


Blush v0.4/v0.5/v0.6
---------------

v0.4 - v0.6 are identical (git user error)

Thickened the end wall back to original (pre-v0.3) dimension.


Blush v0.7
----------

Text depth reduced to 0.5mm. (Used two sketch profiles to accommodate)

Enlarged bore radius from 8.42 to 8.65 so that print will be approx 16.8mm,
which is just smaller than the 17mm reamer I intend to use.

Blush v0.8
----------


Rounded the roof by revolving the baffle sketch instead of extruding it.
(Baffle sketch moved 0.5mm in process)
This is intended to provide a constant thickness tip rail.

Text depth increased to 1.0mm because the 0.5mm is lightly noticeable

Blush v0.9 (discontinued...moving on with Blush v0.8.2 instead)
----------

Blush v0.9
- Rounded window bottom
- Wide rails at start, thin rails near tip
- Some restructuring in order to clean up the discontinuity from bore to
loft.

Now uses three lofts total instead of three lofts (and a revolution for
the bore)

Moved near-window-base sketch toward table to make end wall thinner.
In theory it's the same cavity size, but it looks smaller..

Smaller text on version number (6mm instead of 8mm)
Added a "v" to version number


Lofts between these four sketches:
1. Beyond tip rail (roughly rectangular)
2. Near Window Base (15.20mm tall, 16.20mm wide ellipse)
   (Note the position of the ellipse was moved down from previous versions in order
   to make a thinner end wall. Note this movement probably means keeping the same baffle shape as
   before will still sound different...so I'll be checking if the baffle gives the same sound as before.)
   (I liked the sound of v0.8 a lot...but some changes were needed to allow a round window bottom)
   (and to clean up the small discontinuity inside the bore)
3. End of Bore (17.3mm circle) (Note these actually print about 16.9mm for me)
4. Below Bore (17.3mm circle)

Somehow we lost the rail thickness (not fully constrained sketch perhaps?)
So added thickness to lower part of rails to compensate.
Purposefully made rails thinner near tip rail as requested.


Blush v0.8.3
------------

Goal is to shorten so that it can slide onto neck another 10mm
since my new embouchure is playing flatter.

- Shortened by 10mm overall
- Shortened body by 6m, and last bore by 4mm
- Left fin as-is (but can by stylized in the future)


Blush v0.8.4
------------

- Improved vertical stabilizer to look similar to 0.8.2
  but with a touch less round-off at the back
- Smaller text for Blush and version number

