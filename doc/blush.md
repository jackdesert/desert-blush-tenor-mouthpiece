Blush Play Testing
=====================

Jan 13 v0.2 or so
------

cheek_v0.1 started the night strong, but then would not pop test or play well.
But v0.2 (air quotes around it because I'm not exactly sure which one it is) plays well!
Big and dark.
My cheeks get tired playing it, not sure why/how.

Today we took the big photo with all 30 prototypes. (30 prototypes in 2 weeks, since
we started on mouthpiece design Jan 01)
(Previous to that I worked on the alto perch for a week)
(Previous to that I was drawing the alto perch but the printer was in the mail)

Tomorrow it's gonna snow!!! And Saundra's making soup :) :) :)


Jan 14
------

Playing blush  "0.2" which is actually a couple versions before 0.1.
I've been trying to figure out how to make it easier/more responsive to play.
So far what helps is to push hard!! (Maybe it's a new reed, IDK.)
And sometimes it really sings.
(It always sings the low notes...sometimes it does okay up high too.)

Push Push Push!!
