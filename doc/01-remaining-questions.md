Remaining Questions
===================

How to Print Sharp Edges
------------------------

Side rails need to be either accurate, or be extra tall
so that after we sand them down to the correct height
then they still have the correct width.


How much cheek is helpful
-------------------------

Are cheeks helpful?
- so far no!

What shape of cheek is helpful
------------------------------

- One use of the cheek is to increase the width of the side rails
  whilst leaving the baffle alone

How to control the shape of the baffle
--------------------------------------

Precision control over the baffle would allow subtle changes in sound
- Precision control over the lateral edge of the baffle would allow
  you to DRAW exactly the side rail that you want. (printing it is a different matter)

How to use a git branching model with CAD
-----------------------------------------

I would like to add and subtract features without affecting mainline.

I would like to be able to reliable add a feature, then remove it,
and know that the model is unchanged.

I would like to add A, add B, then remove A, and know that the
model is the same as if I had only added B
