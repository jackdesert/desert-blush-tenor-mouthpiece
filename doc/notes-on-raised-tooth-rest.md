Notes on Raised Table
=====================

Made a version of Blush v0.4 with the tooth rest 2mm higher.

The normal version sounds brighter to me.
The tooth-rest-higher version I seem to leak air out the sides.
But as an experiment for Andy I think it will be useful
