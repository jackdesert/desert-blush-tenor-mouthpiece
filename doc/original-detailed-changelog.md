Gregory (copy of Getasax Mkii 7star)
=======

v0.1 (January 1, 2024)
----

Used measurements only


v0.2
----

### Dimension A
Expected: 25.4mm; v0.1 Test Print: 24.1mm
Increased radius to 12.4

### Dimension B

Increased radius from 15.3 to 16.05

### Dimension E
Expected: 26.7 diam, V0.1 Test print: 26.5
Increasing radius from 13.7 to 13.75
(But drawing initially said 14.447)
So increased to 14.5

v0.3
----

Decrease bore from 8.6 to 8.5 (Based on JACK__ print settings from v0.2)

v0.4
----

Decrease dimension E to be just bigger than v0.1.

v0.5
----

Decreased the 60mm height to E from erroneous 73mm back to correct 60mm.

v0.8.2
------

Updated crowning radius (where your teeth rest) to 30mm based on measurement with 60mm diameter ring

v0.8.3
------

Used a tidy polyfit curve for facing

v0.8.4
------

Clean tip rail

v0.8.5
------

Reduced neck opening to radius 8.5mm instead of 8.6

Challenges
----------

At v0.8.5 I made several prints and attempted to play some of them.
At this state it looks like a mouthpiece on the outside, but the baffle looks
more like a chess piece.

### Reed Seal

So far I have not gotten the reed to pass a pop test. Options:

1. Measure mouthpiece before sanding to see if the design dimensions
   are coming out correctly in the print
2. Try the 0.4mm nozzle
3. Use medium sandpaper (instead of fine) to aggressively file it down to make
   sure we are beneath the print texture
4. Learn how to make a flat table (read the Intro by Keith)
5. Try a different reed
6. Try playing with a softer reed
7. Make the window narrower and make sure the length is correct

### Neck Seal

I have a half round file that can be used to mostly remove the inner seam.
I'm still not pleased with the roughness inside the . Options:

1. Heat up a 12mm socket and run that inside bore attached to a drill
   (Or maybe just by hand would work if it were warm)


### Dimension Consistency

When print settings change, the dimensions of the final product change.
Jeremy says layer height and something else will affect this. Options:

1. Choose print settings now and work around them. or
2. Keep a log of what dimensions actually matter, so you can modify those dimensions
   easily later if you decide to change the print settings


### Seam

The seam is awkward when it shows up next to the table. Options:

1. Permanently move the seal to the rear in some saved print/printer setting
2. File down the inner bore
3. Acquire a 17mm steel rod. Head it up and use it to iron
4. Purchase a 17mm drill bit or reamer and use it after printing

### Dark Sound

this is to be expected when there is a negative baffle.
Not sure if this is affecting how it plays.
Not expecting this to affect reed seal.

### Rough on Teeth

The PLA is rough to my top teeth. Options:

1. Play with double-lipped embouchure until we get a prototype that seals
2. sand and use a mouthpiece patch
3. Order mouthpiece patches in BULK

0.9.2
-----

This version has a gentle rollover baffle (instead of the chess piece)
Since I am focused on getting two mouthpiece prints to seal this weekend,
I have been focusing on the table and the rails.

The print I made (desert-prusa, seam at rear) is smoother than many.
It will definitely need sandpaper to remove the surface texture.

### Rails Lower than Table

The rails do not extend tangentially from the table plane.
Actually, the rails are misformed.
The rails are thin compared to the extrusion width
*Can extrusion width be changed?*

I like the idea of a thin rail, but the rail must come up high enough
to match the table.

### Window too wide

Somehow the window is the same width at the bottom as at the top.
(It appears the top measurement was used for both)

### Shape and Control of Loft Tunnel

I previously thought the loft would be sharp edges, meaning straight lines
when viewed from the side.

### Side roughness of Rails

(Meditation gem)

The roughness of the rails on the sides may be preventing the reed from sealing,
since the reed flexes in multiple directions.

Try this: Smooth the sides of the rails. (Inside side probably does not matter...
I expect the outside face to matter because of the direction of curvature of the reed)

v0.9.3
------

Narrower window base

v0.9.4
------

Narrower bore

### Lofty Lofts (Use multiple lofts)

Using a multi-plane loft, you get the most control at the two ends.
That is where you can specify the launch weight and launch angle.

Here are the things I really want from those lofts:

1. Accurate, easy to read width of the window at the botton and top
   Because 0.9.4 has a window too wide for the reed
2. Full control over the rollover baffle shape

My plan for the next print is to use separate lofts for each section.
That way there will be no accommodating curves unless we intend them that way.

Other aspects have been solved...the tunnel from the bore to the chamber and
forming the rollover baffle is the consciousness we are looking for now.
It could be done in one step or in multiple..

For example, we could do a simple extrusion (or revolution) to create the rollover baffle.
But it needs to change width as it goes...

I wonder if we can create multiple planes and box

v0.9.6
------

### Multiple Simple Lofts
This version uses multiple simple lofts.
It also measures the window opening


Within the last three we changed to an angle for the cone shape.
0.9.6 has angle 86.6 (90-3.4)
Test printing 87.50 (still too narrow at tip)
Test printing 88.5

v0.9.7 (Plays after 10 seconds of sanding) Jan 7, 2024
------------------------------------------


### Plays after 10 seconds of sanding

After 10 seconds of sanding on 320 grit, and holding the reed on with my fingers,
Playing GABC and GABC w octave Plays and Plays Well!

(Later spent 30-45 minutes cleaning it up and dialing it in)


### Ligature Loose (Length Difference)

Ligature is still loose...

I would like to simplify the dimensioning of the cone that forms
the most basic shape of the mouthpiece body. That is, constrain it
to use a particular angle.

Not sure how to set the intercept...I've been using the butt
of the mouthpiece as the origin, but one could make the shank longer
and leave the crucial dimensions unchanged..

LENGTH
It turns out that v0.9.7 is longer than the GS MkII by about  since ever
GS MkII: 105mm tip to tail
v0.9.7: 108.8mm

Since they are the same width near the base, this extra length means that where you intend to
put the ligature it's too loose. And if You slide it down to where the ligature fits better,
then the ligature is a long way from the base of the window (harder to seal).

The window is also longer overall.

Goal: Redraw with Z=0 being the farthest extent of the tip rail away from the bore.

This is intended to ensure that the crucial dimensions are mapped together
(window length, height of tooth rest, facing curve)
while other dimensions can be changed more or less orthogonally (length of bore, for example)


### Sanding and matching Original

Order of Operations:

1. Sand the sides of the side rails and the sides of the tip rail
   with 320, then 600 grit. It's okay to be rounded about it...
   the final finishing will happen on the glass.
   Make sure they shine!!

2. Flatten the table with 320 (could have used 200...it took about ten minutes)

3. Use size 0.05, 0.5, 0.63, 0.75, 0.88. 1,0, 1.25, 2.0 (mm) feeler gages
   to match the profile to the original.


### What's it Like to Play v0.9.7

It is responsive and does have a little bit of color---moreso when you put what
FEELS LIKE a lot of mouthpiece in your mouth. I say "feels like" because the thick
tooth rest is 3mm taller than it needs to be, so your teeth have to be quite wide to get the usual
amount of insertion.

It's darker (less high end partials,  not as loud overall) than the getasax Mkii.
It is responsive to play.

### Next Steps

1. Get ligature to fit well using angle with z=0 at tip rail
2. Get tooth rest to fit well using angle with z=0 at tip rail.
3. Enlarge bore just enough to make it fit like the original
4. Learn more about lofts and other ways of making 3D paths.
5. Open the chamber so that it becomes rounded as soon as the rollover baffle is completed
6. Publish instructions on how to hand finish it
7. Write out the dimensions for the entire mouthpiece on paper??
   (At least the bore, the body angles)


v0.10.0__tip_rail_at_z0
---------------------------

Started moving everything in the negative Z direction by -108.51mm.
(Initial sketch)

v0.10.1__all_moved_neg_z
----------------------------

The entire model appears to have been successfully moved negative Z.


### Next Steps
1. Make it so it's 105mm total length
   - Plan is to shorten the distane between "window full width" and z=0
1. Make the chamber round as possible
3. Get tooth rest to fit well using angle with z=0 at tip rail.
4. Enlarge bore just enough to make it fit like the original
5. Learn more about lofts and other ways of making 3D paths.
6. Open the chamber so that it becomes rounded as soon as the rollover baffle is completed
7. Publish instructions on how to hand finish it
8. Write out the dimensions for the entire mouthpiece on paper??
   (At least the bore, the body angles)

v0.10.2
-------

Adjusted overall length to be 105mm
  - Decreased distance from full open window to 0Z
  - Adjusted dimensions near bottom so everything rolls up
Increased bore radius from 8.4mm to 8.42mm because previous model is a bit tight.
*XXX* Let's find out how much change is the difference from tight neck cork to loose.


### Next Steps
0. Shorten window
1. Make the chamber round as possible
2. Get tooth rest to fit well using angle with z=0 at tip rail.
3. Learn more about lofts and other ways of making 3D paths.
4. Open the chamber so that it becomes rounded as soon as the rollover baffle is completed
5. Publish instructions on how to hand finish it
6. Write out the dimensions for the entire mouthpiece on paper??
   (At least the bore, the body angles)

v0.10.3__improved_chin
----------------------

### Changes
- Chin (tooth rest) is now defined by angles
- Adjusted angle so that there is plenty of material at end of rollover baffle
- Increased revolve radius so tip rail is not pinched

### Next Steps
1. Make the chamber round as possible
2. Learn more about lofts and other ways of making 3D paths.
3. Open the chamber so that it becomes rounded as soon as the rollover baffle is completed
4. Publish instructions on how to hand finish it
5. Write out the dimensions for the entire mouthpiece on paper??
   (At least the bore, the body angles)

v0.10.4__rounded_chamber
------------------------

### Changes

Rounded two profiles used in lofts so that the chamber is rounded instead of flat-sided.


### Printed!

This time I used 100% infill with concentric pattern. (wish this was captured in source control)


### Next Steps
1. Move forehead back to the same dimension where GS MkII is
   because this one was so close to the tip that my upper lip runs into it
2. Thicken the tooth support. (So thin you can see through it)
   (Probably double it and test print just the top
1. Make window narrower to match GS MkII
2. Learn more about lofts and other ways of making 3D paths.
3. Publish instructions on how to hand finish it
4. Write out the dimensions for the entire mouthpiece on paper??
   (At least the bore, the body angles)


v0.10.5__thicker_tooth_rest
---------------------------

### Changes
- tooth rest is 2mm instead of 1mm
- Chin is 40mm in Z direction from tip to clearn upper lip
- Now using "directional" again for topmost loft, as this gives it
  somewhat of a rollover baffle.

### Observations after test print of beak end
- Incline of tooth ramp is steeper than GS MkII
- GS MkII has an oval bore following the rollover baffle, whereas v0.10.5 has a round one
- Spacing between side rails could be narrower to be optimal
- Rollover baffle is back! (It seems any time you edit the profiles for a loft, you have to
                            re-select the "direction" option)
- If there were an easy way to glue halves together, I would cut off the v0.10.4 print I have
  and glue it to the v0.10.5 beak I printed


v0.10.6_tailfin
---------------

Ambivalent about decorations, but here is a simple tailfin.


### Bringing the table up

Before rotate:
29.41 at base of cone (goal is 29.91)

10.01 from center at window (goal is 11.31)


Try 1:
  11.79 at window
  30.517 at base

rotated -1.15 degrees


9.193 (needs 11.31

4.7 + 1.5

v0.
v0.10.9__16mm_window
------------

This is the first one with a tail that got printed.

### Successes

A. The ligature fits!!!
B. The tail looks good!

### Challenges with Final Finishing
1. the rails are thin
2. the rails sit lower than the table when printed
3. Required a LOT of sanding to get the table to extend to the break point
4. The LOT of sanding may have contributed to the bright sound, as it makes
   the baffle higher when upside down

### How does it sound

It's brighter than the previous playable one ()
(Still needs gage work)

v0.10.10 Cheeks
---------------

Added cheeks.
that makes more side area where we blow.
Actually swelled it out so that the ligature will have to slip OVER it,
which might be a good thing...you'll know where the ligature goes :)

### Comments on the cheeks

Blush v0.3
----------

### Changes (from git log)

- Thinner end wall (dubious...I liked it better before)
- 6 degree angle on the "tip rail" extrusion cut which limits the overall length.
  This was intended to reduce the width of the tip rail, which it did



### Playing
- Brighter than Blush "0.2" which is actually like "-1"
  I wonder if this is the new rail...I don't expect the end wall change to have
  manifested that much
- Freer blowing than Blush "0.2" which is actually like "-1"
  Tomorrow I'll put a proper bite pad on it and give it a fair shake

### Needs

- The "Beyond tip rail" sketch could have a rounded belly so that
  the new tip rail is constant width from left to right
- Print one with the new tip rail that does NOT have the extra fill-in baffle
  (meaning the baffle would be lower). For comparison

Jan 16 "Why won't it play an more"
----------------------------------

I have two mouthpieces printed in Overture PLA Cream. They played at one time,
but now they don't play so well.

Checking the table, it is now convex.

Printing two copies of blush 0.4, one in Overture PLA Professional,
one in eSun PLA+. Goals:

- maintains its dimensions
- easily sandable (not gummy)
