Version System
==============

How to Use
----------

1. Open a file in Fusion360. The filename should not have any version numbers.
2. Make relevant changes to the model.
3. In the terminal, run `version [filename]` to determine which version number to use
4. Update the sketch containing the version number. (Use the version number from previous step)
5. Export to .f3d format, right over top of the file you opened.
6. Git commit, and include the version number in your comment


Background
----------

Each model currently under development (or mailine)
has a single filename in the root directory. This makes
it straightforward to export to the same filename each time
(No need to provide a version number when exporting)

version() Bash Function
-----------------------
    # Bash function to compute a decimal version number.
    # If this would be the third git commit, the version
    # number will be 0.3
    # If this would be the seventeenth git commit, the version
    # number will be 1.7
    version() {
        # Check if a filename is provided as an argument
        if [ -z "$1" ]; then
            echo "Usage: version <filename>"
            return 1
        fi

        local FILENAME="$1"

        # Get the number of Git commits for the specified filename
        local NUM_COMMITS=$(git log --oneline -- "$FILENAME" | wc -l)

        # Increment the number of commits by 1 to start from version 1
        ((NUM_COMMITS++))

        # Calculate the tens and ones place
        local TENS_PLACE=$((NUM_COMMITS / 10))
        local ONES_PLACE=$((NUM_COMMITS % 10))

        # Print the computed version number in the format 'a.b'
        echo "Version $TENS_PLACE.$ONES_PLACE"
    }


