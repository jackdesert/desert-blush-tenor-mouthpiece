Andy
====

The Original Andy (Happy Accident)
-----------------

commit 09865f6542e5527abcf0301f8112d25b77dabaeb
Author: jack <jack>
Date:   Sun Jan 7 21:46:43 2024 -0600

    v0.9.7

A       gregory/v0.9.7__better-tip-rail.f3d

Note this was in the mainline before the word "andy" was added.

### Features
- 108.5mm long (longer than intended)
- Stubby when viewed from front because tooth rest is so tall
- It's so tall that it FEELS like you're putting a LOT of mouthpiece in your mouth
- This was nicknamed "Andy" because Wally has a hypothesis that andy is putting too much mouthpiece
  in his mouth, and the tall tooth rest makes it so you have to really mean it if you want to get a lot in
- 17mm bore along with 17mm tall at "window full width"
- Chamber becomes mostly rectangular (flat on the sides, curved one one face, nearly flat on a fourth)
  (Which end is the bottom anyway?)
- Narrow-ish side rails (about 1mm at the thinnest part, about 1.5mm at each end)
- Table is lower than optimal (needs a shim atop the reed in order for the ligature to tighten up)
- Cone measured from the bottom, so that's another reason the ligature seems too loose.

### Playing
- A throaty sound that is not shrill
- Leaks air and saliva around the sides
- The more mouthpiece in your mouth, the better it sounds. (And it FEELS like a lot)



andy__v0.1.3__fixed_cone_expanded_bore.f3d  (Andy v2)
------------------------------------------

commit 60bb231af0a794b58537b5245b3d90693cbcb461 (HEAD -> main)
Author: jack <jack>
Date:   Fri Jan 12 13:35:32 2024 -0600

    andy__v0.1.3

A       gregory/andy__v0.1.3__fixed_cone_expanded_bore.f3d


Note the name "andy" has been adopted and the version number restarted.
(That's why the version number appears less than its parent)

We could call this "Andy v2"

This builds on the original andy by cleaning up a few features.
This is the one sent to PJ.

### Changes from original_andy

- 106mm tall (intended to be 105...what happened?)
- tooth rest lowered
- table raised and rotated to match newer designs
- outer cone raised 3mm in Z direction, which means ligature will be snug
- Broadened the bore to match newer designs
- As a result of the outer cone being expanded and the table being raised,
  the rails are wider. I don't love the rails being wider...I aspire to crisp, thin
  rails but I need to figure out how to allow that to happen with a 3D emulsion printer.
  (loft and bore remain the same)
- Recording available at jack/videos/54.1-3d__andy_v0.1.3__ship-to-PJ.mov

### Playing

It's a deep, gruff tone.
I've been playing the original andy and this one for a whole week it seems,
and now the WCW64 and the GSMkii seem shrill (at least in the upper end) by comparison.
It does squeak sometimes.
And it complains when I push it too hard. Like jumping in on a low C# from pause,
it shudders a bit. (I wonder if this is because the facing curve is impure)
But it has a fat, round sound.

### Review from PJ

I sent one to PJ (actually two---one finished by me, one fresh out of the printer)
Technically, the one I sent him fresh out of the printer was printed at 1/3 speed, but actually
looked slightly worse than the normal print. (Normal being desert-prusa with concentric 100% fill
and seams at the back)

