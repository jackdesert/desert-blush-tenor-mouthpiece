Things to Tackle Next
=====================

1. Learn to make a curved, accurate baffle that also curves to make the tip rail constant width
2. Learn to print thin, accurate rails
3. Choose a make/model of material that holds its dimensions and has the colors/textures I like
