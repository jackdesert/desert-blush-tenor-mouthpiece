Which Versions Sound the Best (In order: best at the top)
=============================

Blush v0.3
----------

Blush v0.3 is moderate to blow and had just a touch of brightness.

Previous to this version, models would sometimes produce fantastic sound
(especially in the lower registers)
but you really had to push. (Highly resistant)

I assume the biggest change is the narrow tip, effected
by the angle on the tip rail extrusion cut.

We will know for sure after blush v0.4 prints,
which is the same as v0.4 but with the standard thick end wall.

(v0.3 has a slightly longer window because the thin end wall broke off)


Blush "0.2" that is actually one or two revisions before 0.1
-------------------------------

(Was gregory_v0.10.9_c_another_cheek)

(need to write down this is the one we printed)
16mm bore, rounded on inside
This responds better than what I sent PJ
The upper notes seem to work better if I push a LOT LOT of air


v0.9.7 (Andy)
-------------

This one is too tall (tooth rest) and air leaks out the side.
It is also 3.5mm longer than intended, as a happy accident.
It has the richest tone so far.

Best reed position: 0.0mm or 0.2mm underhang.




v0.10.9 (16mm bore & tail but no cheeks)
----------------------------------------

Tied for second place
This one will be developed further.
Next quest is to get the rails to print squarely. (Temperature? Speed?)
Because this one had to be sanded down so far in order
to get the rails tangent to the table. Then the baffle ended up shorter
than expected.

The tip rail ended up thicker than expected. (a whopping 4.0mm)
It sounds the best with about the reed at 2.5mm underhang,
which gives it an effective tip rail of 1.5mm



v0.10.10 (16mm bore & tail & cheeks)
------------------------------------

Tied for second place

The bottom end of this one is really phat!
Digging this sound with the reed down 1mm from far extent of tip rail.

Looks so cute!
BUT I do get tired playing it (leaks out the bottom/sides, so have to use a different form)
