Desert Blush
============

A 3D modeled tenor saxophone mouthpiece.

See https://www.printables.com/model/722520-blush-7-tenor-saxophone-mouthpiece

Design Approach
----------------

1. Take external measurements from GS MkII 7*
2. Use basic shapes to form the main cone, the tooth rest, and the table
3. Use multiple lofts to form the chamber (lofts are quick ways to make tunnels
   but hard to make them accurate.)
4. Tweak the sketches comprising the chamber to get the inside edges
   of the rails to have the correct measurement. (If too wide, reed will not seal)
5. Ensure constant width side rails by amending the original body cone.
   (Constant width gives the printer a better chance of actually bringing
   the rail out to meet the table plane. Imagine if the rail grew super skinny
   along its length, the printer would skimp right there and the rail would
   not reach the table plane.)
   Note in this design, this is a thin version of one prototype named "Cheeks"
   The cheeks are where the body sweels outside of the main cone
6. Fine tune the main cone angle and the table cut so the ligature fits
7. Fine tune the baffle area of the chamber by using a sketch with specific points
   and revolving it until it hits the side of the chamber. (If you curve your baffle,
   then it makes it easy to have a constant width tip rail.)
8. Add text for model and version


Challenges
----------

### Uneven baffle

1. When I get done hand finishing one, the facing curve has reached the passenger
side of the mouthpiece more than it has reached the driver side.
(Assuming mouthpiece drives tip first, like a rocket)


Currently Working On
--------------------
1. Repeatability. Want to be able to make three mouthpieces that play the same.

2. Creep (working on annealing)

Next Things to Tackle
---------------------

1. Get baffle to be even when you are done hand finishing.
   This might need a different hand finishing approach,
   or the print might not be symmetric. (Asymmetric rails?)

2. Rounded baffle so it meets the tip rail nicely

3. Uniform bore (have a 17mm reamer...)




Requests from Customers
-----------------------

1. Make it more forgiving of lateral reed placement.

